package com.eshop.model.accounts.bank;

import com.eshop.model.PersistentObject;
import com.eshop.model.accounts.Account;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "banks")
public class Bank
        extends PersistentObject {

    @OneToOne
    private Account owner;
    private Long money;

    public Bank() {
        //default constructor
    }

    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }

        Bank bank = (Bank) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getOwner(), bank.getOwner())
                .append(getMoney(), bank.getMoney())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getOwner())
                .append(getMoney())
                .toHashCode();
    }
}
