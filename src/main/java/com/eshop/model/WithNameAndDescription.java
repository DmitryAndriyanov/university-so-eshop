package com.eshop.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.Hibernate;

import javax.persistence.MappedSuperclass;

/**
 * Base class for Entities with field name and field description
 */
@MappedSuperclass
public abstract class WithNameAndDescription
        extends WithName {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }

        if (Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }

        WithNameAndDescription that = (WithNameAndDescription) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getDescription(), that.getDescription())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getDescription())
                .toHashCode();
    }
}
