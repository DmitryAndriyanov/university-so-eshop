package com.eshop.model.shop;

import com.eshop.model.PersistentObject;
import com.eshop.model.accounts.Account;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "buckets")
public class Bucket
        extends PersistentObject {

    @ManyToOne
    private Account account;

    @ManyToOne
    private Product product;

    private Date addDate;

    public Bucket() {
        //default constructor
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }

        Bucket bucket = (Bucket) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getAccount(), bucket.getAccount())
                .append(getProduct(), bucket.getProduct())
                .append(getAddDate(), bucket.getAddDate())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getAccount())
                .append(getProduct())
                .append(getAddDate())
                .toHashCode();
    }
}
