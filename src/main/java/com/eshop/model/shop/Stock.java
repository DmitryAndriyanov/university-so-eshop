package com.eshop.model.shop;

import com.eshop.model.BaseForShop;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Info about stock in eshop
 */
@Entity
@Table(name = "stocks")
public class Stock
        extends BaseForShop {

    private Date startDay;

    private Date endDate;

    @ManyToMany
    @JoinTable(name = "stocks_products",
            joinColumns = @JoinColumn(name = "stock_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id")
    )
    private Set<Product> products = new HashSet<>();

    public Date getStartDay() {
        return startDay;
    }

    public void setStartDay(Date startDay) {
        this.startDay = startDay;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }

        if (Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }

        Stock stock = (Stock) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getStartDay(), stock.getStartDay())
                .append(getEndDate(), stock.getEndDate())
                .append(getProducts(), stock.getProducts())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getStartDay())
                .append(getEndDate())
                .append(getProducts())
                .toHashCode();
    }
}
