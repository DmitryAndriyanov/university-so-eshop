package com.eshop.model.shop;

import com.eshop.model.WithNameAndDescription;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * Info about tag for products in eshop
 */
@Entity
@Table(name = "tags")
public class Tag
        extends WithNameAndDescription {

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "tags")
    private Set<Product> products = new HashSet<>();

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }

        if (Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }

        Tag tag = (Tag) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getProducts(), tag.getProducts())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getProducts())
                .toHashCode();
    }
}
