package com.eshop.model.system;

import com.eshop.model.WithName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Info about media files for entities
 */
@Entity
@Table(name = "media_files")
public class MediaFile
        extends WithName {

    private String displayName;

    private String type;

    private String path;


    public MediaFile() {
        //default constructor
    }

    /**
     * Create media file
     *
     * @param name        name
     * @param displayName display name
     * @param type        type
     * @param path        path
     */
    public MediaFile(String name, String displayName, String type, String path) {
        setName(name);
        this.displayName = displayName;
        this.type = type;
        this.path = path;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MediaFile mediaFile = (MediaFile) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getDisplayName(), mediaFile.getDisplayName())
                .append(getType(), mediaFile.getType())
                .append(getPath(), mediaFile.getPath())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getDisplayName())
                .append(getType())
                .append(getPath())
                .toHashCode();
    }
}
