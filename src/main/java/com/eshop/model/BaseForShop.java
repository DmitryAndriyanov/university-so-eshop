package com.eshop.model;

import com.eshop.model.system.MediaFile;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.Hibernate;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

/**
 * Base class for EShop Entities
 */
@MappedSuperclass
public abstract class BaseForShop
        extends WithNameAndDescription {
    @OneToOne
    private MediaFile photo;

    public MediaFile getPhoto() {
        return photo;
    }

    public void setPhoto(MediaFile photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }

        if (Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }

        BaseForShop that = (BaseForShop) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getPhoto(), that.getPhoto())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getPhoto())
                .toHashCode();
    }
}
