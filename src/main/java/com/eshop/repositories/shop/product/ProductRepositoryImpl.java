package com.eshop.repositories.shop.product;

import com.eshop.filters.shop.ProductFilter;
import com.eshop.model.shop.Product;
import com.eshop.repositories.BaseRepositoryImpl;
import com.querydsl.core.BooleanBuilder;

/**
 * Implementation for ProductRepository
 */
public class ProductRepositoryImpl
        extends BaseRepositoryImpl<Product, ProductFilter>
        implements ProductRepositoryCustom {

    /**
     * Default constructor
     */
    public ProductRepositoryImpl() {
        super(Product.class);
    }

    @Override
    protected BooleanBuilder getQueryPredicate(ProductFilter searchFilter) {
        return null;
    }
}
