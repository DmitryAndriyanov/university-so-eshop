package com.eshop.repositories.shop.product;

import com.eshop.filters.shop.ProductFilter;
import com.eshop.model.shop.Product;
import com.eshop.repositories.BaseRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.Set;

/**
 * Default repository for products
 */
public interface ProductRepository
        extends JpaRepository<Product, Long>,
        QueryDslPredicateExecutor<Product>,
        ProductRepositoryCustom {
    Set<Product> findByIdIn(Set<Long> productIds);
}

interface ProductRepositoryCustom
        extends BaseRepositoryCustom<Product, ProductFilter> {
}
