package com.eshop.repositories.shop.tag;

import com.eshop.filters.shop.TagFilter;
import com.eshop.model.shop.Tag;
import com.eshop.repositories.BaseRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.Set;

/**
 * Default repository for tags
 */
public interface TagRepository
        extends JpaRepository<Tag, Long>,
        QueryDslPredicateExecutor<Tag>,
        TagRepositoryCustom {

    /**
     * Find all tags with id in set
     *
     * @param tagIds set with tags ids
     * @return Set<Tag>, tags
     */
    Set<Tag> findByIdIn(Set<Long> tagIds);
}

interface TagRepositoryCustom
        extends BaseRepositoryCustom<Tag, TagFilter> {
}
