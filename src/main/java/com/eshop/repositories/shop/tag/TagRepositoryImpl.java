package com.eshop.repositories.shop.tag;

import com.eshop.filters.shop.TagFilter;
import com.eshop.model.shop.Tag;
import com.eshop.repositories.BaseRepositoryImpl;
import com.querydsl.core.BooleanBuilder;

/**
 * Implementation for TagRepository
 */
public class TagRepositoryImpl
        extends BaseRepositoryImpl<Tag, TagFilter>
        implements TagRepositoryCustom {

    /**
     * Default constructor
     */
    public TagRepositoryImpl() {
        super(Tag.class);
    }

    @Override
    protected BooleanBuilder getQueryPredicate(TagFilter searchFilter) {
        return null;
    }
}
