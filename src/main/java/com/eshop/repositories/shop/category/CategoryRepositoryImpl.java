package com.eshop.repositories.shop.category;

import com.eshop.filters.shop.CategoryFilter;
import com.eshop.model.shop.Category;
import com.eshop.repositories.BaseRepositoryImpl;
import com.querydsl.core.BooleanBuilder;

/**
 * Implementation for CategoryRepository
 */
public class CategoryRepositoryImpl
        extends BaseRepositoryImpl<Category, CategoryFilter>
        implements CategoryRepositoryCustom {

    /**
     * Default constructor
     */
    public CategoryRepositoryImpl() {
        super(Category.class);
    }

    @Override
    protected BooleanBuilder getQueryPredicate(CategoryFilter searchFilter) {
        return null;
    }
}
