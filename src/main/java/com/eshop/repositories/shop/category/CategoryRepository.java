package com.eshop.repositories.shop.category;

import com.eshop.filters.shop.CategoryFilter;
import com.eshop.model.shop.Category;
import com.eshop.repositories.BaseRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.List;

/**
 * Default repository for categories
 */
public interface CategoryRepository
        extends JpaRepository<Category, Long>,
        QueryDslPredicateExecutor<Category>,
        CategoryRepositoryCustom {
    List<Category> findByNameAndDescription(String name, String description);
}

interface CategoryRepositoryCustom
        extends BaseRepositoryCustom<Category, CategoryFilter> {

}
