package com.eshop.repositories.shop.bucket;

import com.eshop.filters.shop.BucketFilter;
import com.eshop.model.shop.Bucket;
import com.eshop.repositories.BaseRepositoryImpl;
import com.querydsl.core.BooleanBuilder;

/**
 * Implementation for BucketRepository
 */
public class BucketRepositoryImpl
        extends BaseRepositoryImpl<Bucket, BucketFilter>
        implements BucketRepositoryCustom {

    /**
     * Default constructor
     */
    public BucketRepositoryImpl() {
        super(Bucket.class);
    }

    @Override
    protected BooleanBuilder getQueryPredicate(BucketFilter searchFilter) {
        return null;
    }
}
