package com.eshop.repositories.shop.bucket;

import com.eshop.filters.shop.BucketFilter;
import com.eshop.model.shop.Bucket;
import com.eshop.repositories.BaseRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.List;

/**
 * Default repository for categories
 */
public interface BucketRepository
        extends JpaRepository<Bucket, Long>,
        QueryDslPredicateExecutor<Bucket>,
        BucketRepositoryCustom {
    List<Bucket> findByAccountId(Long accountId);
}

interface BucketRepositoryCustom
        extends BaseRepositoryCustom<Bucket, BucketFilter> {
}
