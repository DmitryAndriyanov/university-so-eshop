package com.eshop.repositories.shop.stock;

import com.eshop.filters.shop.StockFilter;
import com.eshop.model.shop.Stock;
import com.eshop.repositories.BaseRepositoryImpl;
import com.querydsl.core.BooleanBuilder;

/**
 * Implementation for StockRepository
 */
public class StockRepositoryImpl
        extends BaseRepositoryImpl<Stock, StockFilter>
        implements StockRepositoryCustom {

    /**
     * Default constructor
     */
    public StockRepositoryImpl() {
        super(Stock.class);
    }

    @Override
    protected BooleanBuilder getQueryPredicate(StockFilter searchFilter) {
        return null;
    }
}
