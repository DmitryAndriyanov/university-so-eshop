package com.eshop.repositories.shop.stock;

import com.eshop.filters.shop.StockFilter;
import com.eshop.model.shop.Stock;
import com.eshop.repositories.BaseRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Default repository for stocks
 */
public interface StockRepository
        extends JpaRepository<Stock, Long>,
        QueryDslPredicateExecutor<Stock>,
        StockRepositoryCustom {
}

interface StockRepositoryCustom
        extends BaseRepositoryCustom<Stock, StockFilter> {
}
