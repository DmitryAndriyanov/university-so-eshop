package com.eshop.repositories.account.bank;

import com.eshop.filters.account.BankFilter;
import com.eshop.model.accounts.bank.Bank;
import com.eshop.repositories.BaseRepositoryImpl;
import com.querydsl.core.BooleanBuilder;

/**
 * Account repository impl
 */
public class BankRepositoryImpl
        extends BaseRepositoryImpl<Bank, BankFilter>
        implements BankRepositoryCustom {

    /**
     * Base constructor
     */
    public BankRepositoryImpl() {
        super(Bank.class);
    }

    @Override
    protected BooleanBuilder getQueryPredicate(BankFilter searchFilter) {
        return null;
    }
}
