package com.eshop.repositories.account.bank;

import com.eshop.filters.account.BankFilter;
import com.eshop.model.accounts.bank.Bank;
import com.eshop.repositories.BaseRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Repository for Account
 */
public interface BankRepository
        extends JpaRepository<Bank, Long>,
        QueryDslPredicateExecutor<Bank>,
        BankRepositoryCustom {

}

interface BankRepositoryCustom
        extends BaseRepositoryCustom<Bank, BankFilter> {
}
