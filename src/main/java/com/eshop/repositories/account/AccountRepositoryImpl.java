package com.eshop.repositories.account;

import com.eshop.filters.account.AccountFilter;
import com.eshop.model.accounts.Account;
import com.eshop.repositories.BaseRepositoryImpl;
import com.querydsl.core.BooleanBuilder;

/**
 * Account repository impl
 */
public class AccountRepositoryImpl
        extends BaseRepositoryImpl<Account, AccountFilter>
        implements AccountRepositoryCustom {

    /**
     * Base constructor
     */
    public AccountRepositoryImpl() {
        super(Account.class);
    }

    @Override
    protected BooleanBuilder getQueryPredicate(AccountFilter searchFilter) {
        return null;
    }
}
