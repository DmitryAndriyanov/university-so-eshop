package com.eshop.services;


import com.eshop.filters.account.AccountFilter;
import com.eshop.model.accounts.Account;
import com.eshop.repositories.BaseRepositoryCustom;
import com.eshop.repositories.account.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


/**
 * Service for account
 */
@Service
public class AccountService
        implements BaseCRUDService<Account, AccountFilter> {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public JpaRepository<Account, Long> getRepository() {
        return accountRepository;
    }

    @Override
    public BaseRepositoryCustom<Account, AccountFilter> getRepositoryCustom() {
        return accountRepository;
    }

    /**
     * Find Account by username
     *
     * @param username username
     * @return Account
     */
    public Account findAccountByUsername(String username) {
        return accountRepository.findAccountByUsername(username);
    }

    public Account getCurrentAccount() {
        return accountRepository.findAccountByUsername(getCurrentAccountUsername());
    }

    private String getCurrentAccountUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
