package com.eshop.services;

import com.eshop.filters.account.BankFilter;
import com.eshop.model.accounts.bank.Bank;
import com.eshop.repositories.BaseRepositoryCustom;
import com.eshop.repositories.account.bank.BankRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class BankService
        implements BaseCRUDService<Bank, BankFilter> {

    @Autowired
    private BankRepository repository;

    /**
     * Get JpaRepository<E,Long>
     *
     * @return JpaRepository<E,Long>
     */
    @Override
    public JpaRepository<Bank, Long> getRepository() {
        return repository;
    }

    /**
     * Get BaseRepository<E,F>
     *
     * @return BaseRepository<E,F>
     */
    @Override
    public BaseRepositoryCustom<Bank, BankFilter> getRepositoryCustom() {
        return repository;
    }
}
