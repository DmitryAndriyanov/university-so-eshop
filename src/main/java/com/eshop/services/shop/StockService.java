package com.eshop.services.shop;

import com.eshop.filters.shop.StockFilter;
import com.eshop.model.shop.Stock;
import com.eshop.repositories.BaseRepositoryCustom;
import com.eshop.repositories.shop.stock.StockRepository;
import com.eshop.services.BaseCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 * Service for categories
 */
@Service
public class StockService
        implements BaseCRUDService<Stock, StockFilter> {

    @Autowired
    private StockRepository repository;

    /**
     * Get JpaRepository<E,Long>
     *
     * @return JpaRepository<E,Long>
     */
    @Override
    public JpaRepository<Stock, Long> getRepository() {
        return repository;
    }

    /**
     * Get BaseRepository<E,F>
     *
     * @return BaseRepository<E,F>
     */
    @Override
    public BaseRepositoryCustom<Stock, StockFilter> getRepositoryCustom() {
        return repository;
    }
}
