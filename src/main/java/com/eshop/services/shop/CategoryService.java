package com.eshop.services.shop;

import com.eshop.filters.shop.CategoryFilter;
import com.eshop.model.shop.Category;
import com.eshop.repositories.BaseRepositoryCustom;
import com.eshop.repositories.shop.category.CategoryRepository;
import com.eshop.services.BaseCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 * Service for categories
 */
@Service
public class CategoryService
        implements BaseCRUDService<Category, CategoryFilter> {

    @Autowired
    private CategoryRepository repository;

    /**
     * Get JpaRepository<E, Long>
     *
     * @return JpaRepository<E,Long>
     */
    @Override
    public JpaRepository<Category, Long> getRepository() {
        return repository;
    }

    /**
     * Get BaseRepository<E,F>
     *
     * @return BaseRepository<E,F>
     */
    @Override
    public BaseRepositoryCustom<Category, CategoryFilter> getRepositoryCustom() {
        return repository;
    }
}
