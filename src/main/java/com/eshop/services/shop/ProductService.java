package com.eshop.services.shop;

import com.eshop.filters.shop.ProductFilter;
import com.eshop.model.shop.Product;
import com.eshop.repositories.BaseRepositoryCustom;
import com.eshop.repositories.shop.product.ProductRepository;
import com.eshop.services.BaseCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Service for categories
 */
@Service
public class ProductService
        implements BaseCRUDService<Product, ProductFilter> {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private TagService tagService;

    @Autowired
    private CategoryService categoryService;

    /**
     * Get JpaRepository<E,Long>
     *
     * @return JpaRepository<E,Long>
     */
    @Override
    public JpaRepository<Product, Long> getRepository() {
        return repository;
    }

    /**
     * Get BaseRepository<E,F>
     *
     * @return BaseRepository<E,F>
     */
    @Override
    public BaseRepositoryCustom<Product, ProductFilter> getRepositoryCustom() {
        return repository;
    }

    public Set<Product> findByIdIn(Set<Long> productIds) {
        return repository.findByIdIn(productIds);
    }
}
