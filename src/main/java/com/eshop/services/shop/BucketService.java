package com.eshop.services.shop;

import com.eshop.filters.shop.BucketFilter;
import com.eshop.model.accounts.Account;
import com.eshop.model.shop.Bucket;
import com.eshop.model.shop.Product;
import com.eshop.repositories.BaseRepositoryCustom;
import com.eshop.repositories.shop.bucket.BucketRepository;
import com.eshop.services.AccountService;
import com.eshop.services.BaseCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BucketService
        implements BaseCRUDService<Bucket, BucketFilter> {

    @Autowired
    private BucketRepository repository;

    @Autowired
    private ProductService productService;

    @Autowired
    private AccountService accountService;

    /**
     * Get JpaRepository<E,Long>
     *
     * @return JpaRepository<E,Long>
     */
    @Override
    public JpaRepository<Bucket, Long> getRepository() {
        return repository;
    }

    /**
     * Get BaseRepository<E,F>
     *
     * @return BaseRepository<E,F>
     */
    @Override
    public BaseRepositoryCustom<Bucket, BucketFilter> getRepositoryCustom() {
        return repository;
    }

    public void addProductToBucket(Long productId) {
        Product product = productService.findOne(productId);
        Account account = accountService.getCurrentAccount();

        Bucket bucket = new Bucket();
        bucket.setAccount(account);
        bucket.setProduct(product);
        bucket.setAddDate(new Date());
        save(bucket);
    }

    public List<Bucket> findAllForAccount(Long accountId) {
        return repository.findByAccountId(accountId);
    }

    public void deleteProductFromBucket(Long bucketId) {
        delete(bucketId);
    }
}

