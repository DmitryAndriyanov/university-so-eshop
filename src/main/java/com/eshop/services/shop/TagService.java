package com.eshop.services.shop;

import com.eshop.filters.shop.TagFilter;
import com.eshop.model.shop.Tag;
import com.eshop.repositories.BaseRepositoryCustom;
import com.eshop.repositories.shop.tag.TagRepository;
import com.eshop.services.BaseCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Service for categories
 */
@Service
public class TagService
        implements BaseCRUDService<Tag, TagFilter> {

    @Autowired
    private TagRepository repository;

    /**
     * Get JpaRepository<E,Long>
     *
     * @return JpaRepository<E,Long>
     */
    @Override
    public JpaRepository<Tag, Long> getRepository() {
        return repository;
    }

    /**
     * Get BaseRepository<E,F>
     *
     * @return BaseRepository<E,F>
     */
    @Override
    public BaseRepositoryCustom<Tag, TagFilter> getRepositoryCustom() {
        return repository;
    }

    /**
     * Find all tags with id in set
     *
     * @param tagIds set with tags ids
     * @return Set<Tag>, tags
     */
    public Set<Tag> findByIdIn(Set<Long> tagIds) {
        return repository.findByIdIn(tagIds);
    }
}
