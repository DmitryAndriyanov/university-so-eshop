package com.eshop.mapping.mappers.shop.stock;

import com.eshop.dto.shop.stock.StockDTO;
import com.eshop.mapping.converters.SetLongToProductsConverter;
import com.eshop.model.shop.Stock;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

/**
 * Map StockDTO to Stock
 */
@Component
public class StockAddDTOMapper
        extends StockDTOMapper<StockDTO> {

    @Override
    protected PropertyMap<StockDTO, Stock> getDTOToEntityMapping() {

        return new PropertyMap<StockDTO, Stock>() {
            @Override
            protected void configure() {
                using(getConverter(SetLongToProductsConverter.class))
                        .map(source.getProducts())
                        .setProducts(null);
            }
        };
    }
}