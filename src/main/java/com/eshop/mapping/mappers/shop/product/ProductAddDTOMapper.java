package com.eshop.mapping.mappers.shop.product;

import com.eshop.dto.shop.product.ProductDTO;
import com.eshop.mapping.converters.LongToCategoryConverter;
import com.eshop.mapping.converters.SetLongToTagsConverter;
import com.eshop.model.shop.Product;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

/**
 * Map ProductDTO to Product
 */
@Component
public class ProductAddDTOMapper
        extends ProductDTOMapper<ProductDTO> {

    @Override
    protected PropertyMap<ProductDTO, Product> getDTOToEntityMapping() {

        return new PropertyMap<ProductDTO, Product>() {
            @Override
            protected void configure() {
                using(getConverter(LongToCategoryConverter.class))
                        .map(source.getCategory())
                        .setCategory(null);
                using(getConverter(SetLongToTagsConverter.class))
                        .map(source.getTags())
                        .setTags(null);
            }
        };
    }
}