package com.eshop.mapping.mappers.shop.stock;

import com.eshop.dto.BaseDTO;
import com.eshop.filters.shop.StockFilter;
import com.eshop.mapping.SimpleDTOMapper;
import com.eshop.model.shop.Stock;
import com.eshop.services.BaseCRUDService;
import com.eshop.services.shop.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Mapper Stock
 *
 * @param <D> info about stock from client
 */
@Component
public class StockDTOMapper<D extends BaseDTO<Stock>>
        extends SimpleDTOMapper<Stock, D> {

    @Autowired
    private StockService stockService;

    @Override
    protected BaseCRUDService<Stock, StockFilter> getEntityProviderService() {
        return stockService;
    }
}
