package com.eshop.mapping.mappers.shop.product;

import com.eshop.dto.shop.product.ProductViewDTO;
import com.eshop.mapping.converters.CategoryToLongConverter;
import com.eshop.model.shop.Product;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

/**
 * Map Product to ProductViewDTOl
 */
@Component
public class ProductViewGetDTOMapper
        extends ProductDTOMapper<ProductViewDTO> {

    @Override
    protected PropertyMap<Product, ProductViewDTO> getEntityToDTOMapping() {
        return new PropertyMap<Product, ProductViewDTO>() {
            @Override
            protected void configure() {
                using(getConverter(CategoryToLongConverter.class))
                        .map(source.getCategory())
                        .setCategory(null);
            }
        };
    }
}
