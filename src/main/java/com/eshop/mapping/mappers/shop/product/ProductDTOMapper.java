package com.eshop.mapping.mappers.shop.product;

import com.eshop.dto.BaseDTO;
import com.eshop.filters.shop.ProductFilter;
import com.eshop.mapping.SimpleDTOMapper;
import com.eshop.model.shop.Product;
import com.eshop.services.BaseCRUDService;
import com.eshop.services.shop.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Mapper Product
 *
 * @param <D> info about product from client
 */
@Component
public class ProductDTOMapper<D extends BaseDTO<Product>>
        extends SimpleDTOMapper<Product, D> {

    @Autowired
    private ProductService productService;

    @Override
    protected BaseCRUDService<Product, ProductFilter> getEntityProviderService() {
        return productService;
    }
}
