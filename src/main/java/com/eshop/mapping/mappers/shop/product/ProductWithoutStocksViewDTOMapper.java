package com.eshop.mapping.mappers.shop.product;


import com.eshop.dto.shop.product.ProductWithoutStocksViewDTO;
import com.eshop.mapping.converters.CategoryToLongConverter;
import com.eshop.model.shop.Product;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;


@Component
public class ProductWithoutStocksViewDTOMapper
        extends ProductDTOMapper<ProductWithoutStocksViewDTO> {

    @Override
    protected PropertyMap<Product, ProductWithoutStocksViewDTO> getEntityToDTOMapping() {
        return new PropertyMap<Product, ProductWithoutStocksViewDTO>() {
            @Override
            protected void configure() {
                using(getConverter(CategoryToLongConverter.class))
                        .map(source.getCategory())
                        .setCategory(null);
            }
        };
    }
}
