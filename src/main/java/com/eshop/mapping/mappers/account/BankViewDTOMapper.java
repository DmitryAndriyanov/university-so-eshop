package com.eshop.mapping.mappers.account;

import com.eshop.dto.account.bank.BankViewDTO;
import com.eshop.mapping.converters.AccountToUsernameConverter;
import com.eshop.model.accounts.bank.Bank;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

/**
 * Map Bank to BankViewDTO
 */
@Component
public class BankViewDTOMapper
        extends BankDTOMapper<BankViewDTO> {

    @Override
    protected PropertyMap<Bank, BankViewDTO> getEntityToDTOMapping() {
        return new PropertyMap<Bank, BankViewDTO>() {
            @Override
            protected void configure() {
                using(getConverter(AccountToUsernameConverter.class))
                        .map(source.getOwner())
                        .setOwner(null);
            }
        };
    }
}