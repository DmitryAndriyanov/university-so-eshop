package com.eshop.mapping.mappers.account;

import com.eshop.dto.BaseDTO;
import com.eshop.filters.account.BankFilter;
import com.eshop.mapping.SimpleDTOMapper;
import com.eshop.model.accounts.bank.Bank;
import com.eshop.services.BankService;
import com.eshop.services.BaseCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Mapper Bank
 *
 * @param <D> info about bank from client
 */
@Component
public class BankDTOMapper<D extends BaseDTO<Bank>>
        extends SimpleDTOMapper<Bank, D> {

    @Autowired
    private BankService service;

    @Override
    protected BaseCRUDService<Bank, BankFilter> getEntityProviderService() {
        return service;
    }
}
