package com.eshop.mapping.converters;

import com.eshop.model.shop.Product;
import com.eshop.services.shop.ProductService;
import org.modelmapper.AbstractConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Convert Set<Long> tagIds to Set<Tag> tags
 */
@Component
public class SetLongToProductsConverter
        extends AbstractConverter<Set<Long>, Set<Product>> {

    @Autowired
    private ProductService productService;

    @Override
    protected Set<Product> convert(Set<Long> tagIds) {
        return productService.findByIdIn(tagIds);
    }
}