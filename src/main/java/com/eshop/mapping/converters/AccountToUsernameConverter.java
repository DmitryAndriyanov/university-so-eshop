package com.eshop.mapping.converters;

import com.eshop.model.accounts.Account;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

/**
 * Convert Account to username
 */
@Component
public class AccountToUsernameConverter
        extends AbstractConverter<Account, String> {
    @Override
    protected String convert(Account account) {
        return account.getUsername();
    }
}
