package com.eshop.mapping.converters;

import com.eshop.model.shop.Category;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

/**
 * Convert Category to category id
 */
@Component
public class CategoryToLongConverter
        extends AbstractConverter<Category, Long> {
    @Override
    protected Long convert(Category category) {
        return category.getId();
    }
}
