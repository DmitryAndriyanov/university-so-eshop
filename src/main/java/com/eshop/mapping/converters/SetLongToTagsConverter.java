package com.eshop.mapping.converters;

import com.eshop.model.shop.Tag;
import com.eshop.services.shop.TagService;
import org.modelmapper.AbstractConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Convert Set<Long> tagIds to Set<Tag> tags
 */
@Component
public class SetLongToTagsConverter
        extends AbstractConverter<Set<Long>, Set<Tag>> {

    @Autowired
    private TagService tagService;

    @Override
    protected Set<Tag> convert(Set<Long> tagIds) {
        return tagService.findByIdIn(tagIds);
    }
}