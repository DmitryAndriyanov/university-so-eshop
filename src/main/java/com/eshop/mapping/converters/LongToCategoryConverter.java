package com.eshop.mapping.converters;

import com.eshop.api.IBaseCRUDOperations;
import com.eshop.filters.shop.CategoryFilter;
import com.eshop.model.shop.Category;
import com.eshop.services.BaseCRUDService;
import com.eshop.services.shop.CategoryService;
import org.modelmapper.AbstractConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Convert category id to Category
 */
@Component
public class LongToCategoryConverter
        extends AbstractConverter<Long, Category>
        implements IBaseCRUDOperations<Category, CategoryFilter> {

    @Autowired
    private CategoryService categoryService;

    /**
     * Get service for entity
     *
     * @return BaseCRUDService<E,F>, service for entity
     */
    @Override
    public BaseCRUDService<Category, CategoryFilter> getService() {
        return categoryService;
    }

    @Override
    protected Category convert(Long categoryId) {
        return findOne(categoryId);
    }
}
