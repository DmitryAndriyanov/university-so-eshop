package com.eshop.dto;

import com.eshop.model.WithNameAndDescription;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Base DTO for those who has fields name and description
 *
 * @param <T> entity extends WithName
 */
public abstract class InfoDTO<T extends WithNameAndDescription>
        extends WithNameDTO<T> {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof InfoDTO)) {
            return false;
        }

        InfoDTO<?> that = (InfoDTO<?>) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getDescription(), that.getDescription())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getDescription())
                .toHashCode();
    }
}