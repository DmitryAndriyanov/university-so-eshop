package com.eshop.dto.shop.bucket;

import com.eshop.dto.shop.product.ProductWithoutStocksViewDTO;
import com.eshop.model.shop.Bucket;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Date;

public class BucketViewDTO
        extends BaseBucketDTO {

    private ProductWithoutStocksViewDTO product;
    private Date addDate;

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    public ProductWithoutStocksViewDTO getProduct() {
        return product;
    }

    public void setProduct(ProductWithoutStocksViewDTO product) {
        this.product = product;
    }

    @Override
    protected Class<Bucket> getEntityClass() {
        return Bucket.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof BucketViewDTO)) {
            return false;
        }

        BucketViewDTO that = (BucketViewDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getAddDate(), that.getAddDate())
                .append(getProduct(), that.getProduct())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getAddDate())
                .append(getProduct())
                .toHashCode();
    }
}
