package com.eshop.dto.shop.bucket;

import com.eshop.dto.BaseDTO;
import com.eshop.model.shop.Bucket;

public abstract class BaseBucketDTO
        extends BaseDTO<Bucket> {

    @Override
    protected Class<Bucket> getEntityClass() {
        return Bucket.class;
    }
}
