package com.eshop.dto.shop.tag;

import com.eshop.dto.InfoDTO;
import com.eshop.model.shop.Tag;

/**
 * DTO for tag entity
 */
public class TagDTO
        extends InfoDTO<Tag> {
    @Override
    protected Class<Tag> getEntityClass() {
        return Tag.class;
    }
}
