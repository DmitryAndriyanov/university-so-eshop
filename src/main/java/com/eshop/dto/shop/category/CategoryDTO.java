package com.eshop.dto.shop.category;

import com.eshop.dto.InfoDTO;
import com.eshop.model.shop.Category;

/**
 * DTO for category entity
 */
public class CategoryDTO
        extends InfoDTO<Category> {

    @Override
    protected Class<Category> getEntityClass() {
        return Category.class;
    }
}
