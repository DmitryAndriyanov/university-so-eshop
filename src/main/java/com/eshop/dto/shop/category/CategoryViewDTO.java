package com.eshop.dto.shop.category;

import com.eshop.dto.InfoDTO;
import com.eshop.dto.shop.product.ProductViewDTO;
import com.eshop.dto.system.MediaFileViewDTO;
import com.eshop.model.shop.Category;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Set;

/**
 * ViewDTO for category entity
 */
public class CategoryViewDTO
        extends InfoDTO<Category> {

    private MediaFileViewDTO photo;

    private Set<ProductViewDTO> products;

    public Set<ProductViewDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductViewDTO> products) {
        this.products = products;
    }

    public MediaFileViewDTO getPhoto() {
        return photo;
    }

    public void setPhoto(MediaFileViewDTO photo) {
        this.photo = photo;
    }

    @Override
    protected Class<Category> getEntityClass() {
        return Category.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof CategoryViewDTO)) {
            return false;
        }

        CategoryViewDTO that = (CategoryViewDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getPhoto(), that.getPhoto())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getPhoto())
                .toHashCode();
    }
}
