package com.eshop.dto.shop.stock;

import com.eshop.dto.system.MediaFileViewDTO;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class StockWithNoProductViewDTO
        extends BaseStockDTO {
    private MediaFileViewDTO photo;

    public MediaFileViewDTO getPhoto() {
        return photo;
    }

    public void setPhoto(MediaFileViewDTO photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof StockWithNoProductViewDTO)) {
            return false;
        }

        StockWithNoProductViewDTO that = (StockWithNoProductViewDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getPhoto(), that.getPhoto())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getPhoto())
                .toHashCode();
    }
}
