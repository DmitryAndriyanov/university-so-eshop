package com.eshop.dto.shop.stock;

import com.eshop.dto.shop.product.ProductWithoutStocksViewDTO;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Set;

/**
 * ViewDTO for category entity
 */
public class StockViewDTO
        extends StockWithNoProductViewDTO {

    private Set<ProductWithoutStocksViewDTO> products;

    public Set<ProductWithoutStocksViewDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductWithoutStocksViewDTO> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof StockViewDTO)) {
            return false;
        }

        StockViewDTO that = (StockViewDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getProducts(), that.getProducts())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getProducts())
                .toHashCode();
    }
}
