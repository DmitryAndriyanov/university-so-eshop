package com.eshop.dto.shop.stock;

import com.eshop.dto.InfoDTO;
import com.eshop.model.shop.Stock;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.sql.Date;

/**
 * Base DTO for StockDTOs
 */
public abstract class BaseStockDTO
        extends InfoDTO<Stock> {

    private Date startDay;

    private Date endDate;

    public Date getStartDay() {
        return startDay;
    }

    public void setStartDay(Date startDay) {
        this.startDay = startDay;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    protected Class<Stock> getEntityClass() {
        return Stock.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof BaseStockDTO)) {
            return false;
        }

        BaseStockDTO that = (BaseStockDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getStartDay(), that.getStartDay())
                .append(getEndDate(), that.getEndDate())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getStartDay())
                .append(getEndDate())
                .toHashCode();
    }
}
