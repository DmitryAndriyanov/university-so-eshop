package com.eshop.dto.shop.stock;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Set;

/**
 * DTO for stock entity
 */
public class StockDTO
        extends BaseStockDTO {

    private Set<Long> products;

    public Set<Long> getProducts() {
        return products;
    }

    public void setProducts(Set<Long> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof StockDTO)) {
            return false;
        }

        StockDTO stockDTO = (StockDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getProducts(), stockDTO.getProducts())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getProducts())
                .toHashCode();
    }
}
