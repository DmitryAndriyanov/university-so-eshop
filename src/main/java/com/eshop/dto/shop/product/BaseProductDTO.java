package com.eshop.dto.shop.product;

import com.eshop.dto.InfoDTO;
import com.eshop.model.shop.Product;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Base for ProductDTOs
 */
public abstract class BaseProductDTO
        extends InfoDTO<Product> {

    private Long price;

    private Long category;

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }

    @Override
    protected Class<Product> getEntityClass() {
        return Product.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof BaseProductDTO)) {
            return false;
        }

        BaseProductDTO that = (BaseProductDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getPrice(), that.getPrice())
                .append(getCategory(), that.getCategory())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getPrice())
                .append(getCategory())
                .toHashCode();
    }
}
