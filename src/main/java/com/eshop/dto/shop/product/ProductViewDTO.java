package com.eshop.dto.shop.product;

import com.eshop.dto.shop.stock.StockWithNoProductViewDTO;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Set;

/**
 * ViewDTO for category entity
 */
public class ProductViewDTO
        extends ProductWithoutStocksViewDTO {

    private Set<StockWithNoProductViewDTO> stocks;

    public Set<StockWithNoProductViewDTO> getStocks() {
        return stocks;
    }

    public void setStocks(Set<StockWithNoProductViewDTO> stocks) {
        this.stocks = stocks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof ProductViewDTO)) {
            return false;
        }

        ProductViewDTO that = (ProductViewDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getStocks(), that.getStocks())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getStocks())
                .toHashCode();
    }
}
