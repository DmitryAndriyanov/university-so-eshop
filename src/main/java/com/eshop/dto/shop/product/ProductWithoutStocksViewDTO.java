package com.eshop.dto.shop.product;

import com.eshop.dto.shop.tag.TagViewDTO;
import com.eshop.dto.system.MediaFileViewDTO;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Set;

public class ProductWithoutStocksViewDTO
        extends BaseProductDTO {

    private MediaFileViewDTO photo;

    private Set<TagViewDTO> tags;

    public MediaFileViewDTO getPhoto() {
        return photo;
    }

    public void setPhoto(MediaFileViewDTO photo) {
        this.photo = photo;
    }

    public Set<TagViewDTO> getTags() {
        return tags;
    }

    public void setTags(Set<TagViewDTO> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof ProductWithoutStocksViewDTO)) {
            return false;
        }

        ProductWithoutStocksViewDTO that = (ProductWithoutStocksViewDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getPhoto(), that.getPhoto())
                .append(getTags(), that.getTags())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getPhoto())
                .append(getTags())
                .toHashCode();
    }
}
