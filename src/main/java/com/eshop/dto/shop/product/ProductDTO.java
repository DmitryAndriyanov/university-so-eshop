package com.eshop.dto.shop.product;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Set;

/**
 * DTO for product entity
 */
public class ProductDTO
        extends BaseProductDTO {

    private Set<Long> tags;

    public Set<Long> getTags() {
        return tags;
    }

    public void setTags(Set<Long> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof ProductDTO)) {
            return false;
        }

        ProductDTO that = (ProductDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getTags(), that.getTags())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getTags())
                .toHashCode();
    }
}
