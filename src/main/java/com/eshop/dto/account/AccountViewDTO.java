package com.eshop.dto.account;

import com.eshop.dto.BaseDTO;
import com.eshop.model.accounts.Account;
import com.eshop.model.accounts.Role;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class AccountViewDTO
        extends BaseDTO<Account> {

    private String username;

    private String password;

    private Role userRole;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getUserRole() {
        return userRole;
    }

    public void setUserRole(Role userRole) {
        this.userRole = userRole;
    }

    @Override
    protected Class<Account> getEntityClass() {
        return Account.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof AccountViewDTO)) {
            return false;
        }

        AccountViewDTO that = (AccountViewDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getUsername(), that.getUsername())
                .append(getPassword(), that.getPassword())
                .append(getUserRole(), that.getUserRole())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getUsername())
                .append(getPassword())
                .append(getUserRole())
                .toHashCode();
    }
}
