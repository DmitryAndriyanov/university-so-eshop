package com.eshop.dto.account.bank;

import com.eshop.dto.BaseDTO;
import com.eshop.model.accounts.bank.Bank;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class BankViewDTO
        extends BaseDTO<Bank> {

    private String owner;

    private Long money;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    @Override
    protected Class<Bank> getEntityClass() {
        return Bank.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof BankViewDTO)) {
            return false;
        }

        BankViewDTO that = (BankViewDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getOwner(), that.getOwner())
                .append(getMoney(), that.getMoney())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getOwner())
                .append(getMoney())
                .toHashCode();
    }
}