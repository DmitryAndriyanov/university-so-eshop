package com.eshop.dto.account.bank;

import com.eshop.dto.BaseDTO;
import com.eshop.model.accounts.bank.Bank;

public class BankDTO
        extends BaseDTO<Bank> {

    @Override
    protected Class<Bank> getEntityClass() {
        return Bank.class;
    }
}
