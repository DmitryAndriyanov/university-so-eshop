package com.eshop.dto;

import com.eshop.model.WithName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Base dto for with name entities
 *
 * @param <T> entity extends WithName
 */
public abstract class WithNameDTO<T extends WithName>
        extends BaseDTO<T> {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WithNameDTO withNameDTO = (WithNameDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getName(), withNameDTO.getName())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getName())
                .toHashCode();
    }
}
