package com.eshop.dto.system;

import com.eshop.dto.WithNameDTO;
import com.eshop.model.system.MediaFile;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * ViewDTO for MediaFile
 */
public class MediaFileViewDTO
        extends WithNameDTO<MediaFile> {

    private String displayName;

    private String type;

    private String path;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MediaFileViewDTO that = (MediaFileViewDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getDisplayName(), that.getDisplayName())
                .append(getType(), that.getType())
                .append(getPath(), that.getPath())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getDisplayName())
                .append(getType())
                .append(getPath())
                .toHashCode();
    }

    @Override
    protected Class<MediaFile> getEntityClass() {
        return MediaFile.class;
    }
}
