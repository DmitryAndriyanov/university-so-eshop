package com.eshop.api.account;

import com.eshop.api.BaseController;
import com.eshop.api.IBaseCRUDOperations;
import com.eshop.dto.account.bank.BankViewDTO;
import com.eshop.filters.account.BankFilter;
import com.eshop.model.accounts.bank.Bank;
import com.eshop.services.BankService;
import com.eshop.services.BaseCRUDService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Bank controller
 */
@Api(value = "banks")
@RequestMapping("api/banks")
@RestController
public class BankController
        extends BaseController
        implements IBaseCRUDOperations<Bank, BankFilter> {

    @Autowired
    private BankService service;

    /**
     * Get service for entity
     *
     * @return BaseCRUDService<E,F>, service for entity
     */
    @Override
    public BaseCRUDService<Bank, BankFilter> getService() {
        return service;
    }

    /**
     * Get info about one bank
     *
     * @param bankId bankId
     * @return Long, info about category to display
     */
    @ApiOperation(value = "View info about category")
    @RequestMapping(value = "/{bankId}", method = RequestMethod.GET)
    public BankViewDTO getOne(
            @ApiParam(
                    name = "bankId", value = "The Id of category", required = true
            )
            @PathVariable Long bankId) {
        Bank bank = findOne(bankId);
        return convertToDto(bank, BankViewDTO.class);
    }

    /**
     * Get info about all banks
     *
     * @param pageable select params
     * @param filter   filter params
     * @return Page<BankViewDTO>, info about all banks to display
     */
    @ApiOperation(value = "View info about banks")
    @RequestMapping(method = RequestMethod.GET)
    public Page<BankViewDTO> getAll(
            @ApiParam(
                    name = "pageable", value = "Request parameters"
            )
                    Pageable pageable,
            @ApiParam(
                    name = "searchParams", value = "search parameters", required = true
            )
            @Valid BankFilter filter) {
        Page<Bank> banks = service.findAll(filter, pageable);
        return banks
                .map(source -> convertToDto(source, BankViewDTO.class));
    }
}
