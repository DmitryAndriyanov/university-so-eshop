package com.eshop.api.account;

import com.eshop.api.BaseController;
import com.eshop.api.IBaseCRUDOperations;
import com.eshop.dto.account.AccountViewDTO;
import com.eshop.filters.account.AccountFilter;
import com.eshop.model.accounts.Account;
import com.eshop.services.AccountService;
import com.eshop.services.BaseCRUDService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "api/accounts")
public class AccountController
        extends BaseController
        implements IBaseCRUDOperations<Account, AccountFilter> {

    @Autowired
    private AccountService accountService;

    @Override
    public BaseCRUDService<Account, AccountFilter> getService() {
        return accountService;
    }

    /**
     * Get info about one account
     *
     * @param accountId accountId
     * @return AccountViewDTO, info about account to display
     */
    @ApiOperation(value = "View info about account")
    @RequestMapping(value = "/{accountId}", method = RequestMethod.GET)
    public AccountViewDTO findAccountById(
            @ApiParam(
                    name = "accountId", value = "The Id of category", required = true
            )
            @PathVariable Long accountId) {
        Account account = findOne(accountId);
        return convertToDto(account, AccountViewDTO.class);
    }

    /**
     * Get info about all accounts
     *
     * @param pageable select params
     * @param filter   filter params
     * @return Page<AccountViewDTO>, info about all accounts to display
     */
    @ApiOperation(value = "View info about accounts")
    @RequestMapping(method = RequestMethod.GET)
    public Page<AccountViewDTO> findAccountsByParameters(
            @ApiParam(
                    name = "pageable", value = "Request parameters"
            )
                    Pageable pageable,
            @ApiParam(
                    name = "searchParams", value = "search parameters", required = true
            )
            @Valid AccountFilter filter) {
        Page<Account> accounts = accountService.findAll(filter, pageable);
        return accounts
                .map(source -> convertToDto(source, AccountViewDTO.class));
    }

    /**
     * Delete info about account
     *
     * @param accountID accountId
     */
    @ApiOperation(value = "Delete info about account")
    @RequestMapping(value = "/{accountId}", method = RequestMethod.DELETE)
    public void deleteCategory(
            @ApiParam(
                    name = "accountId", value = "The Id of account to delete"
            )
            @PathVariable Long accountID) {
        accountService.delete(accountID);
    }
}
