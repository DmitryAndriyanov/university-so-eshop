package com.eshop.api.shop;

import com.eshop.api.BaseController;
import com.eshop.api.IBaseCRUDOperations;
import com.eshop.dto.shop.tag.TagDTO;
import com.eshop.dto.shop.tag.TagViewDTO;
import com.eshop.filters.shop.TagFilter;
import com.eshop.model.shop.Tag;
import com.eshop.services.BaseCRUDService;
import com.eshop.services.shop.TagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Tag controller
 */
@Api(value = "tags")
@RestController
@RequestMapping("api/tags")
public class TagController
        extends BaseController
        implements IBaseCRUDOperations<Tag, TagFilter> {

    @Autowired
    private TagService tagService;

    @Override
    public BaseCRUDService<Tag, TagFilter> getService() {
        return tagService;
    }

    /**
     * Add info about tag
     *
     * @param tagDTO info about tag to add
     * @return TagViewDTO, info about added tag
     */
    @ApiOperation(value = "Add info about tag")
    @RequestMapping(method = RequestMethod.POST)
    public TagViewDTO createTag(
            @ApiParam(
                    name = "tagDTO", value = "info about categories to add"
            )
            @Valid @RequestBody TagDTO tagDTO) {
        tagDTO.setId(null);
        Tag tag = convertToEntity(tagDTO, Tag.class);
        Tag sawedTag = tagService.save(tag);
        return convertToDto(sawedTag, TagViewDTO.class);
    }

    /**
     * Get info about one tag
     *
     * @param tagId tagId
     * @return TagViewDTO, info about tag to display
     */
    @ApiOperation(value = "View info about category")
    @RequestMapping(value = "/{tagId}", method = RequestMethod.GET)
    public TagViewDTO findTagById(
            @ApiParam(
                    name = "tagId", value = "The Id of tag", required = true
            )
            @PathVariable Long tagId) {
        Tag tag = findOne(tagId);
        return convertToDto(tag, TagViewDTO.class);
    }

    /**
     * Get info about all tags
     *
     * @param pageable select params
     * @param filter   filter params
     * @return Page<TagViewDTO>, info about all tags to display
     */
    @ApiOperation(value = "View info about tags")
    @RequestMapping(method = RequestMethod.GET)
    public Page<TagViewDTO> findTagsByParameters(
            @ApiParam(
                    name = "pageable", value = "Request parameters"
            )
                    Pageable pageable,
            @ApiParam(
                    name = "searchParams", value = "search parameters", required = true
            )
            @Valid TagFilter filter) {
        Page<Tag> tags = tagService.findAll(filter, pageable);
        return tags
                .map(source -> convertToDto(source, TagViewDTO.class));
    }

    /**
     * Update info about tag
     *
     * @param tagId  tagId
     * @param tagDTO info for update
     * @return CategoryViewDTO, info about updated tag
     */
    @ApiOperation(value = "Update info about tag")
    @RequestMapping(value = "/{tagId}", method = RequestMethod.PUT)
    public TagViewDTO updateTag(
            @ApiParam(
                    name = "tagId", value = "The Id of tag to update", required = true
            )
            @PathVariable Long tagId,
            @ApiParam(
                    name = "tagDTO", value = "info about tag to update"
            )
            @Valid @RequestBody TagDTO tagDTO) {
        tagDTO.setId(tagId);
        Tag tag = convertToEntity(tagDTO, Tag.class);
        Tag sawedTag = tagService.save(tag);
        return convertToDto(sawedTag, TagViewDTO.class);
    }

    /**
     * Delete info about tag
     *
     * @param tagId tagId
     */
    @ApiOperation(value = "Delete info about tag")
    @RequestMapping(value = "/{tagId}", method = RequestMethod.DELETE)
    public void deleteTag(
            @ApiParam(
                    name = "tagId", value = "The Id of tag to delete"
            )
            @PathVariable Long tagId) {
        tagService.delete(tagId);
    }
}
