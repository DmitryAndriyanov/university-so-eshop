package com.eshop.api.shop;

import com.eshop.api.BaseController;
import com.eshop.api.IBaseCRUDOperations;
import com.eshop.dto.shop.category.CategoryDTO;
import com.eshop.dto.shop.category.CategoryViewDTO;
import com.eshop.filters.shop.CategoryFilter;
import com.eshop.model.shop.Category;
import com.eshop.services.BaseCRUDService;
import com.eshop.services.shop.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Categories controller
 */
@Api(value = "categories")
@RestController
@RequestMapping("api/categories")
public class CategoryController
        extends BaseController
        implements IBaseCRUDOperations<Category, CategoryFilter> {

    @Autowired
    private CategoryService categoryService;

    @Override
    public BaseCRUDService<Category, CategoryFilter> getService() {
        return categoryService;
    }

    /**
     * Add info about category
     *
     * @param categoryDTO info about category to add
     * @return CategoryViewDTO, info about added category
     */
    @ApiOperation(value = "Add info about category")
    @RequestMapping(method = RequestMethod.POST)
    public CategoryViewDTO createCategory(
            @ApiParam(
                    name = "categoryDTO", value = "info about category to add"
            )
            @Valid @RequestBody CategoryDTO categoryDTO) {
        categoryDTO.setId(null);
        Category category = convertToEntity(categoryDTO, Category.class);
        Category sawedCategory = categoryService.save(category);
        return convertToDto(sawedCategory, CategoryViewDTO.class);
    }

    /**
     * Get info about one category
     *
     * @param categoryId categoryId
     * @return CategoryViewDTO, info about category to display
     */
    @ApiOperation(value = "View info about category")
    @RequestMapping(value = "/{categoryId}", method = RequestMethod.GET)
    public CategoryViewDTO findCategoryById(
            @ApiParam(
                    name = "categoryId", value = "The Id of category", required = true
            )
            @PathVariable Long categoryId) {
        Category category = findOne(categoryId);
        return convertToDto(category, CategoryViewDTO.class);
    }

    /**
     * Get info about all categories
     *
     * @param pageable select params
     * @param filter   filter params
     * @return Page<CategoryViewDTO>, info about all categories to display
     */
    @ApiOperation(value = "View info about categories")
    @RequestMapping(method = RequestMethod.GET)
    public Page<CategoryViewDTO> findCategoriesByParameters(
            @ApiParam(
                    name = "pageable", value = "Request parameters"
            )
                    Pageable pageable,
            @ApiParam(
                    name = "searchParams", value = "search parameters", required = true
            )
            @Valid CategoryFilter filter) {
        Page<Category> categories = categoryService.findAll(filter, pageable);
        return categories
                .map(source -> convertToDto(source, CategoryViewDTO.class));
    }

    /**
     * Update info about category
     *
     * @param categoryId  categoryId
     * @param categoryDTO info for update
     * @return CategoryViewDTO, info about updated category
     */
    @ApiOperation(value = "Update info about category")
    @RequestMapping(value = "/{categoryId}", method = RequestMethod.PUT)
    public CategoryViewDTO updateCategory(
            @ApiParam(
                    name = "categoryId", value = "The Id of category to update", required = true
            )
            @PathVariable Long categoryId,
            @ApiParam(
                    name = "categoryDTO", value = "info about category to update"
            )
            @Valid @RequestBody CategoryDTO categoryDTO) {
        categoryDTO.setId(categoryId);
        Category category = convertToEntity(categoryDTO, Category.class);
        Category sawedCategory = categoryService.save(category);
        return convertToDto(sawedCategory, CategoryViewDTO.class);
    }

    /**
     * Delete info about category
     *
     * @param categoryId categoryId
     */
    @ApiOperation(value = "Delete info about category")
    @RequestMapping(value = "/{categoryId}", method = RequestMethod.DELETE)
    public void deleteCategory(
            @ApiParam(
                    name = "categoryId", value = "The Id of category to delete"
            )
            @PathVariable Long categoryId) {
        categoryService.delete(categoryId);
    }
}