package com.eshop.api.shop;

import com.eshop.api.BaseController;
import com.eshop.api.IBaseCRUDOperations;
import com.eshop.dto.shop.bucket.BucketViewDTO;
import com.eshop.filters.shop.BucketFilter;
import com.eshop.model.shop.Bucket;
import com.eshop.services.BaseCRUDService;
import com.eshop.services.shop.BucketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Bucket controller
 */
@Api(value = "buckets")
@RestController
@RequestMapping("api/buckets")
public class BucketController
        extends BaseController
        implements IBaseCRUDOperations<Bucket, BucketFilter> {

    @Autowired
    private BucketService service;

    /**
     * Get service for entity
     *
     * @return BaseCRUDService<E,F>, service for entity
     */
    @Override
    public BaseCRUDService<Bucket, BucketFilter> getService() {
        return service;
    }

    /**
     * Add product to account bucket
     *
     * @param productId added product id
     */
    @ApiOperation(value = "Add product to ")
    @RequestMapping(value = "/products/{productId}", method = RequestMethod.POST)
    public void addProductToBucket(
            @ApiParam(
                    name = "productId", value = "added product id"
            )
            @PathVariable Long productId) {
        service.addProductToBucket(productId);
    }

    /**
     * Get info about account bucket
     *
     * @param accountId accountId
     * @return CategoryViewDTO, info about category to display
     */
    @ApiOperation(value = "View info about category")
    @RequestMapping(value = "/{accountId}", method = RequestMethod.GET)
    public List<BucketViewDTO> findCategoryById(
            @ApiParam(
                    name = "accountId", value = "account id", required = true
            )
            @PathVariable Long accountId) {
        List<Bucket> buckets = service.findAllForAccount(accountId);
        return buckets.stream()
                .map(bucket -> convertToDto(bucket, BucketViewDTO.class))
                .collect(Collectors.toList());
    }

    /**
     * Delete product from account bucket
     *
     * @param bucketId added product id in bucket
     */
    @ApiOperation(value = "Remove product to ")
    @RequestMapping(value = "/{bucketId}", method = RequestMethod.DELETE)
    public void removeProductToBucket(
            @ApiParam(
                    name = "bucketId", value = "bucket id for product"
            )
            @PathVariable Long bucketId) {
        service.deleteProductFromBucket(bucketId);
    }
}