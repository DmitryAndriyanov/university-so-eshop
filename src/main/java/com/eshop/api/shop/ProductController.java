package com.eshop.api.shop;

import com.eshop.api.BaseController;
import com.eshop.api.IBaseCRUDOperations;
import com.eshop.dto.shop.product.ProductDTO;
import com.eshop.dto.shop.product.ProductViewDTO;
import com.eshop.filters.shop.ProductFilter;
import com.eshop.model.shop.Product;
import com.eshop.services.BaseCRUDService;
import com.eshop.services.shop.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Products controller
 */
@Api(value = "products")
@RestController
@RequestMapping("api/products")
public class ProductController
        extends BaseController
        implements IBaseCRUDOperations<Product, ProductFilter> {

    @Autowired
    private ProductService productService;

    @Override
    public BaseCRUDService<Product, ProductFilter> getService() {
        return productService;
    }

    /**
     * Add info about product
     *
     * @param productDTO info about product to add
     * @return ProductViewDTO, info about added product
     */
    @ApiOperation(value = "Add info about product")
    @RequestMapping(method = RequestMethod.POST)
    public ProductViewDTO createProduct(
            @ApiParam(
                    name = "productDTO", value = "info about product to add"
            )
            @Valid @RequestBody ProductDTO productDTO) {
        productDTO.setId(null);
        Product product = convertToEntity(productDTO, Product.class);
        Product sawedProduct = productService.save(product);
        return convertToDto(sawedProduct, ProductViewDTO.class);
    }

    /**
     * Get info about one product
     *
     * @param productId productId
     * @return ProductViewDTO, info about product to display
     */
    @ApiOperation(value = "View info about product")
    @RequestMapping(value = "/{productId}", method = RequestMethod.GET)
    public ProductViewDTO findProductById(
            @ApiParam(
                    name = "productId", value = "The Id of product", required = true
            )
            @PathVariable Long productId) {
        Product product = findOne(productId);
        return convertToDto(product, ProductViewDTO.class);
    }

    /**
     * Get info about all products
     *
     * @param pageable select params
     * @param filter   filter params
     * @return Page<ProductViewDTO>, info about all products to display
     */
    @ApiOperation(value = "View info about products")
    @RequestMapping(method = RequestMethod.GET)
    public Page<ProductViewDTO> findProductsByParameters(
            @ApiParam(
                    name = "pageable", value = "Request parameters"
            )
                    Pageable pageable,
            @ApiParam(
                    name = "searchParams", value = "search parameters", required = true
            )
            @Valid ProductFilter filter) {
        Page<Product> products = productService.findAll(filter, pageable);
        return products
                .map(source -> convertToDto(source, ProductViewDTO.class));
    }

    /**
     * Update info about product
     *
     * @param productId  productId
     * @param productDTO info for update
     * @return ProductViewDTO, info about updated product
     */
    @ApiOperation(value = "Update info about product")
    @RequestMapping(value = "/{productId}", method = RequestMethod.PUT)
    public ProductViewDTO updateProduct(
            @ApiParam(
                    name = "productId", value = "The Id of product to update", required = true
            )
            @PathVariable Long productId,
            @ApiParam(
                    name = "productDTO", value = "info about category to update"
            )
            @Valid @RequestBody ProductDTO productDTO) {
        productDTO.setId(productId);
        Product product = convertToEntity(productDTO, Product.class);
        Product sawedCategory = productService.save(product);
        return convertToDto(sawedCategory, ProductViewDTO.class);
    }

    /**
     * Delete info about product
     *
     * @param productId productId
     */
    @ApiOperation(value = "Delete info about product")
    @RequestMapping(value = "/{productId}", method = RequestMethod.DELETE)
    public void deleteProduct(
            @ApiParam(
                    name = "productId", value = "The Id of product to delete"
            )
            @PathVariable Long productId) {
        productService.delete(productId);
    }
}