package com.eshop.api.shop;

import com.eshop.api.BaseController;
import com.eshop.api.IBaseCRUDOperations;
import com.eshop.dto.shop.stock.StockDTO;
import com.eshop.dto.shop.stock.StockViewDTO;
import com.eshop.filters.shop.StockFilter;
import com.eshop.model.shop.Stock;
import com.eshop.services.BaseCRUDService;
import com.eshop.services.shop.StockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Stocks controller
 */
@Api(value = "stocks")
@RestController
@RequestMapping("api/stocks")
public class StockController
        extends BaseController
        implements IBaseCRUDOperations<Stock, StockFilter> {

    @Autowired
    private StockService stockService;

    @Override
    public BaseCRUDService<Stock, StockFilter> getService() {
        return stockService;
    }

    /**
     * Add info about stock
     *
     * @param stockDTO info about stock to add
     * @return StockViewDTO, info about added stock
     */
    @ApiOperation(value = "Add info about stock")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public StockViewDTO createStock(
            @ApiParam(
                    name = "stockDTO", value = "info about stock to add"
            )
            @Valid @RequestBody StockDTO stockDTO) {
        stockDTO.setId(null);
        Stock stock = convertToEntity(stockDTO, Stock.class);
        Stock sawedStock = stockService.save(stock);
        return convertToDto(sawedStock, StockViewDTO.class);
    }

    /**
     * Get info about one stock
     *
     * @param stockId stockId
     * @return StockViewDTO, info about stock to display
     */
    @ApiOperation(value = "View info about stock")
    @RequestMapping(value = "/{stockId}", method = RequestMethod.GET)
    public StockViewDTO findStockById(
            @ApiParam(
                    name = "stockId", value = "The Id of stock", required = true
            )
            @PathVariable Long stockId) {
        Stock stock = findOne(stockId);
        return convertToDto(stock, StockViewDTO.class);
    }

    /**
     * Get info about all stocks
     *
     * @param pageable select params
     * @param filter   filter params
     * @return Page<StockViewDTO>, info about all stocks to display
     */
    @ApiOperation(value = "View info about stocks")
    @RequestMapping(method = RequestMethod.GET)
    public Page<StockViewDTO> findStocksByParameters(
            @ApiParam(
                    name = "pageable", value = "Request parameters"
            )
                    Pageable pageable,
            @ApiParam(
                    name = "searchParams", value = "search parameters", required = true
            )
            @Valid StockFilter filter) {
        Page<Stock> stocks = stockService.findAll(filter, pageable);
        return stocks
                .map(source -> convertToDto(source, StockViewDTO.class));
    }

    /**
     * Update info about stock
     *
     * @param stockId  stockId
     * @param stockDTO info for update
     * @return StockViewDTO, info about updated stock
     */
    @ApiOperation(value = "Update info about stock")
    @RequestMapping(value = "/{stockId}", method = RequestMethod.PUT)
    public StockViewDTO updateStock(
            @ApiParam(
                    name = "stockId", value = "The Id of stock to update", required = true
            )
            @PathVariable Long stockId,
            @ApiParam(
                    name = "stockDTO", value = "info about stock to update"
            )
            @Valid @RequestBody StockDTO stockDTO) {
        stockDTO.setId(stockId);
        Stock stock = convertToEntity(stockDTO, Stock.class);
        Stock sawedStock = stockService.save(stock);
        return convertToDto(sawedStock, StockViewDTO.class);
    }

    /**
     * Delete info about stock
     *
     * @param stockId stockId
     */
    @ApiOperation(value = "Delete info about stock")
    @RequestMapping(value = "/{stockId}", method = RequestMethod.DELETE)
    public void deleteStock(
            @ApiParam(
                    name = "stockId", value = "The Id of stock to delete"
            )
            @PathVariable Long stockId) {
        stockService.delete(stockId);
    }
}
