package com.eshop.api;

import com.eshop.exceptions.ESEntityNotFoundException;
import com.eshop.filters.SearchFilter;
import com.eshop.model.PersistentObject;
import com.eshop.services.BaseCRUDService;

import java.util.Optional;

/**
 * Interface for CRUD operations
 *
 * @param <E> entity
 * @param <F> filter
 */
@FunctionalInterface
public interface IBaseCRUDOperations<E extends PersistentObject, F extends SearchFilter> {

    /**
     * Get service for entity
     *
     * @return BaseCRUDService<E,F>, service for entity
     */
    BaseCRUDService<E, F> getService();

    /**
     * Find one entity
     *
     * @param id entity id
     * @return Entity
     */
    default E findOne(Long id) {
        return Optional.ofNullable(getService().findOne(id)).orElseThrow(ESEntityNotFoundException::new);
    }
}
