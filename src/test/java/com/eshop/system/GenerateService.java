package com.eshop.system;

import com.eshop.model.shop.Category;
import org.springframework.stereotype.Service;

@Service
public class GenerateService {
    public Category getCategory() {
        Category category = new Category();
        category.setName("test category");
        category.setDescription("test description");
        return category;
    }
}
