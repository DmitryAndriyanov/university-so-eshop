package com.eshop.repositories;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryRepositoryTest {

    //TODO how add h2
//    @Autowired
//    private TestEntityManager entityManager;
//
//    @Autowired
//    private CategoryRepository repository;
//
//    @Autowired
//    private GenerateService service;
//
//    @Test
//    public void findByNameShouldReturnCategory() {
//        Category category = service.getCategory();
//        this.entityManager.persist(category);
//        Category sawedCategory = this.repository
//                .findByNameAndDescription(category.getName(), category.getDescription())
//                .get(0);
//
//        assertEquals(category, sawedCategory);
//    }
}
