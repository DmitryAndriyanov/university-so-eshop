package com.eshop.model;

import com.eshop.model.accounts.Account;
import com.eshop.model.accounts.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AccountTest {
    private Account testAccountU;
    private Account testAccountA;
    private static final String TEST_USERNAME = "test username";
    private static final String TEST_PASSWORD = "test password";

    private Account createMainInfo(){
        Account account = new Account();
        account.setUsername(TEST_USERNAME);
        account.setPassword(TEST_PASSWORD);
        return account;
    }

    @Before
    public void init() {
        testAccountU = createMainInfo();
        testAccountU.setUserRole(Role.ROLE_USER);

        testAccountA = createMainInfo();
        testAccountA.setUserRole(Role.ROLE_ADMIN);
    }

    @After
    public void destroy() {
        testAccountU = null;
        testAccountA = null;
    }

    @Test
    public void baseTestUsername(){
        assertEquals("username must be " + TEST_USERNAME, TEST_USERNAME, testAccountU.getUsername());
    }

    @Test
    public void baseTestPassword(){
        assertEquals("password must be " + TEST_PASSWORD, TEST_PASSWORD, testAccountU.getPassword());
    }

    @Test
    public void baseTestRole(){
        assertEquals("role must be " + Role.ROLE_USER, Role.ROLE_USER, testAccountU.getUserRole());
    }

    @Test
    public void equalsDifferentFalse(){
        assertFalse("user is not an admin", testAccountA.equals(testAccountU));
    }

    @Test
    public void equalsNullFalse(){
        assertFalse("user is not null", testAccountA.equals(null));
    }

    public void equalsTheSame(){
        assertTrue("user is user", testAccountA.equals(testAccountA));
    }
}
